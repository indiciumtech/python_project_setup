# To learn more about how to name dirs, files, classes and functions to be recgonazide as tests to be run:
# https://docs.pytest.org/en/7.1.x/explanation/goodpractices.html#test-discovery
import pytest
from unittest.mock import MagicMock


# Helper functions to show some test cases.
def divide(a, b):
    return a / b


@pytest.fixture
def first_entry():
    return "a"


@pytest.fixture
def order(first_entry):
    return [first_entry]


class TestClassDemoCase:
    """
    When working with tests within the same application scope, it is always interesting to encapsulate them in a class
    in which it is possible to define variables and functions that can be reused.
    """

    def test_demo_default(self):
        """
        The simplest test possible.
        """
        assert True

    @pytest.mark.parametrize(
        "a, b, expected, is_error",
        [
            (1, 1, 1, False),
            (42, 1, 42, False),
            (84, 2, 42, False),
            (42, "b", TypeError, True),
            ("a", 42, TypeError, True),
            (42, 0, ZeroDivisionError, True),
        ],
    )
    def test_demo_parametrize(self, a, b, expected, is_error):
        """
        Test demonstrating parameterization of inputs for multiple scenarios.
        """
        if is_error:
            with pytest.raises(expected):
                divide(a, b)
        else:
            assert divide(a, b) == expected

    def test_demo_fixture_string(self, order):
        """
        Test demonstrating use of fixture in a first scenario.
        """
        order.append("b")
        assert order == ["a", "b"]

    def test_demo_fixture_int(self, order):
        """
        Test demonstrating use of fixture in a second scenario.
        """
        order.append(2)
        assert order == ["a", 2]

    def test_demo_mock(self):
        """
        Test demonstrating how to use mock within a pytest test.
        """
        attr = {"url": "google.com", "ret_code.return_value": "200", "data.return_value": "Some data"}
        api = MagicMock(**attr)
        api.data()

        assert api.ret_code() == "200"
        assert api.url == "google.com"
        api.data.assert_called_once()
