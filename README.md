# Python Project Sample
This repository is intended to serve as a foundation for the development of any python project.

[TOC]

## Repo Structure
***
```
python_project/
│
├── .setup/
│   ├── .flake8
│   ├── .pre-commit-config.yaml
│   ├── black.toml
│   └── project_setup.sh
│
├── src/
│   └── mypkg/
│       └── __init__.py
│       
├── tests/
│   ├── __init__.py
│   └── test_demo.py
│
├── .gitignore
├── requirements.txt
├── pyproject.toml
├── setup.cfg
├── setup.py
├── README.md
```

* `.setup/`: Folder with Indicium specific lint, code formatter, and pre-commit config files.
    * `.flake8`: Custom lint config file.
    *  `.pre-commit-config.yaml`: Custom pre-commit config file that includes black and flake8.
    *  `black.toml`: Custom code formatter config file.
    *  `project_setup.sh`: Script to install lint, code formatter, pre-commit and pytest to run tests.  
* `src/`: Source folder where all python package files should live. 
    * `mypkg*/`: Package folder example.
* `tests*/`: Test folder where all tests files related to the python package should live.
    * `test_demo.py`: File showing how to use pytest to create unit tests.
* `.gitignore`: File that specifies intentionally untracked files that Git should ignore.
* `requirements.txt`: File that specifies what python packages are required to run the project.
* `pyproject.toml`: File that defines which build tool the builder will use to create the package.
* `setup.cfg`: File where all package-related metadata must be defined.
* `setup.py`: File used for builder backcompability.

`*`: *empty init files are used to mark folders to be seen as a package. Don't forget to add one to your package.*

## Minimal Setup Initialization
***

Regardless if you are a new developer introduced to a project already under development or you are starting a new project based on this repository. This step must be performed at the beginning in order to install and configure pre-commit with the lint and code formatter tools.

1. Create a virtual enviroment `venv/` and activate it:
   ```
   python -m venv venv/ && source venv/bin/activate
   ```
2. From **project root** with **venv activated** install the setup:
   ```
   source .setup/project_setup.sh
   ```
3. Check that all tests passed and requirements and pre-commit were installed.

**Done!** Now every `git commit` will trigger pre-commit to run black and flake8 against the code. If the lint or formatter tool fails, then the commit will be aborted.

### Vscode Flake8 Linting
It's possible to integrate flake8 with vscode in order to check interactively if your code is under the constraints defined.

1. Create a `.vscode/` folder in the **project root** with a `settings.json` file in it.
2. Paste the following code into your `settings.json` file. (If you already have a `settings.json` with other definitions, just append it to the file):
   ```
   {
    "python.linting.flake8Enabled": true,
    "python.linting.enabled": true,
    "python.linting.flake8Args": ["--config=.setup/.flake8"]
   }
   ```

**Done!** Now Flake8 will warning you of any errors interactively via Vscode.

## Building Python Package
***

Making a Python package is like making a zip file with metadata. There are multiple tools available to make a package: the most common is setuptools.

### Configuring Metadata Through setup.cfg
`setup.cfg` is the configuration file for setuptools. It tells setuptools about your package (such as the name and version) as well as which code files to include. Eventually much of this configuration may be able to move to `pyproject.toml`.

Inside of `setup.cfg` you will find:
```
[metadata]
name = mypackage
version = 0.0.1

[options]
packages = find:
package_dir =
    =src

[options.packages.find]
where = src
```

There are a [variety of metadata and options supported](https://setuptools.pypa.io/en/latest/userguide/declarative_config.html) here. This is in configparser format; do not place quotes around values. Here is described a relatively minimal set of metadata:

* `metadata`:
    * `name`: is the distribution name of your package. This can be any name as long as it only contains letters, numbers, `_` , and `-`.
    * `version`: is the package version. See [PEP 440](https://peps.python.org/pep-0440/) for more details on versions. You can use `file:` or `attr:` directives to read from a file or package attribute.
* `options`: in this category, we have controls for setuptools itself.
    * `package_dir`: is a mapping of package names and directories. An empty package name represents the **root package** — the directory in the project that contains all python source files for the package — so in this case the **src folder** is designated the root package.
    * `packages`: is a list of all python import packages that should be included in the distribution package. Instead of listing each package manually, we can use the `find:` directive to automatically discover all packages and subpackages and `options.packages.find` to specify the `package_dir` to use.

If you followed the conventions established so far of: organizing your python packages inside the **src folder** and adding an empty `__init__.py` inside each folder that belongs to the package. Then it is possible to proceed to installation of the package.

### Installing Package Locally
For testing purposes, it's a good idea to do a local installation of the package to make sure it's working as expected.
To install the package just execute the following from **project root**:
```
pip install -e .
```
**Done!** Now it's possible to test your python code as a proper python package.

*Don't forget that a installed python package can be imported from anywhere whitin your `venv` because it will be present in the PYTHONPATH. So to test it properly, don't try to import from `src/`, just import it as any other installed python package*

### Building Package for Distribuition
Briefly, there are two ways to distribute a python package: [source distribuition](https://packaging.python.org/en/latest/glossary/#term-Source-Distribution-or-sdist) or [build distribuition](https://packaging.python.org/en/latest/glossary/#term-Built-Distribution). Here we will use the build distribution to generate a `.whl` file that can be installed using `pip`.

A python `.whl` file is essentially a ZIP (`.zip`) archive with a specially crafted filename that tells installers what python versions and platforms the wheel will support.

A wheel is a type of built distribution. In this case, built means that the wheel comes in a ready-to-install format and allows you to skip the build stage required with source distributions.

In order to generate a `.whl` file for your package:

1. Make sure that pip, setuptools and wheel are up to date:
   ```
   pip install --upgrade pip setuptools wheel
   ```
2. Build the wheel:
   ```
   python setup.py bdist_wheel
   ```
3. Make sure that `dist/` and `build/` folder were created.
4. Install the package through `.whl` file presented inside the `dist/` folder.
   ```
   pip install dist/builded-package-example.whl
   ```
5. Run tests against newly installed package to guarantee that it's working fine.

**Done!** Now you can distribute your package just sending the `.whl` file (or hosting it in server to be downloaded) to your co-workers.

*Remember that a `.whl` can be platform and python version specific, so it's installation can failed. But in any case, it's always possible to produce multiples `.whl` files of the same package to cover most of the platforms and python versions.*


## Frequently Asked Questions (FAQ)
***

### Setup.py and setup.cfg?
The `setup.py` and `setup.cfg` files are artefacts of the setuptools module which is designed to help with the packaging process. It is used by pip whose purpose is to install a package either locally or remotely. If we do not configure `setup.py`/`setup.cfg` correctly then pip will not work. In the past we would have written a `setup.py` file which contained a bunch of configuration information but now we should put that configuration information into `setup.cfg` which is effectively an ini file (i.e. does not need to be executed to be read). This is why we now have the minimal `setup.py` file.

### What is pyproject.toml?
The `pyproject.toml` file was introduced in [PEP-518](https://peps.python.org/pep-0518/) as a way of separating configuration of the build system from a specific, optional library (setuptools) and also enabling setuptools to install  itself without already being installed. Subsequently [PEP-621](https://peps.python.org/pep-0621/) introduces the idea that the `pyproject.toml` file be used for wider project configuration and [PEP-660](https://peps.python.org/pep-0660/) proposes finally doing away with the need for `setup.py` for editable installation using `pip`.

### Where do tests go?
Tests go in a tests folder at the top-level of the project with an `__init__.py` file so they are discoverable by applications like pytest. The alternative of placing them inside the `src/mypkg` folder means they will get deployed into production which may not be desirable.

### Why put your package code in a src/ subdirectory?
Using a `src/` directory ensures that you must install a package to test it, so as your users would do. Also it prevents tools like pytest incidently importing it.

## Contribuiting
***
Feel free to fork this repository and propose changes through PRs and issues. It is also possible to create specific branches for framework configurations or similar that derive in some way from the proposed repo model.


## References
***
* [What Are Python Wheels](https://realpython.com/python-wheels/)
* [Packaging Python Projects](https://packaging.python.org/en/latest/tutorials/packaging-projects/#packaging-python-projects)
* [Setuptools](https://setuptools.pypa.io/en/latest/userguide/quickstart.html)
* [Understanding setup.py, setup.cfg and pyproject.toml in Python](https://ianhopkinson.org.uk/2022/02/understanding-setup-py-setup-cfg-and-pyproject-toml-in-python/)
* [A sample Python project by Python Packaging Authority](https://github.com/pypa/sampleproject)